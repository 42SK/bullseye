package x653.bullseye.listenklassen;

/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

public class Node<ContentType> {

    private ContentType content;
    private Node<ContentType> nextNode;
    private Node<ContentType> prevNode;

    protected Node(ContentType pObject) {
        content=pObject;
        nextNode=null;
        prevNode=null;
    }

    protected Node<ContentType> getNext() {
        return nextNode;
    }

    protected Node<ContentType> getPrev() {
        return prevNode;
    }

    protected ContentType getContent() {
        return content;
    }

    protected void setContent(ContentType pObject) {
        content=pObject;
    }

    protected void setNext(Node<ContentType> pNode) {
        this.nextNode=pNode;
        if (pNode!=null) pNode.prevNode=this;
    }

    protected void setPrev(Node<ContentType> pNode) {
        this.nextNode=pNode;
        if (pNode!=null) pNode.nextNode=this;
    }

}
