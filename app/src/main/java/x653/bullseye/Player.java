package x653.bullseye;

import android.view.Gravity;
import android.widget.Toast;

import x653.bullseye.listenklassen.List;

/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

public class Player {
    private final List<Integer> scores;
    private final List<Double> averages;
    private int highest_finish;
    private int total_n;
    private int total_points;
    private MainActivity main;

    protected Player(MainActivity m){
        main=m;
        highest_finish = 0;
        scores=new List<>();
        averages =new List<>();
        total_n = 0;
        total_points = 0;
    }
    protected boolean isEmpty(){
        return scores.isEmpty();
    }
    protected boolean isFinished(){
        return (getTotalScore()==main.getScore());
    }
    protected void clearLastScore(){
        scores.toLast();
        if (scores.hasAccess()){
            total_n--;
            total_points -= scores.getObject();
            scores.remove();
        }
    }
    protected void newLeg(){
        if (isFinished()){
            scores.toLast();
            if (scores.getObject()> highest_finish) highest_finish = scores.getObject();
        }
        averages.append(getAverage());
        for (scores.toFirst();scores.hasAccess();) scores.remove();
    }
    protected void newGame(){
        highest_finish=0;
        total_n=0;
        total_points=0;
        for (averages.toFirst();averages.hasAccess();) averages.remove();
        for (scores.toFirst();scores.hasAccess();) scores.remove();
    }
    protected void score(int i){
        if (((main.getScore()-getTotalScore()-i)<0) || ((main.getScore()-getTotalScore()-i)==1)) {
            i=0;
        }
        if (i==0) {
            Toast toast = Toast.makeText(main, "No Score!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        scores.append(i);
        total_n++;
        total_points +=i;
    }

    protected int getRest(){
        return main.getScore()-getTotalScore();
    }
    protected int getTotalScore(){
        int t = 0;
        for (scores.toFirst();scores.hasAccess();scores.next()){
            t+=scores.getObject();
        }
        return t;
    }
    protected int getTotalN(){
        int t = 0;
        for (scores.toFirst();scores.hasAccess();scores.next()){
            t++;
        }
        return t;
    }
    protected double getAverage(){
        int i=getTotalN();
        if (i==0) return -1;
        return ((double)getTotalScore())/i;
    }
    protected double getTotalAverage(){
        if (total_n ==0) return -1;
        return ((double) total_points)/ total_n;
    }

    protected String getHighest_finish(){
        int i=highest_finish;
        if (isFinished()) {
            scores.toLast();
            if (scores.hasAccess() && (scores.getObject() > highest_finish)) i= scores.getObject();
        }
        if (i==0) return "-";
        return String.format("%d",i);
    }
    protected String getAverages(){
        StringBuilder s=new StringBuilder();
        for (averages.toFirst();averages.hasAccess();averages.next()){
            if (averages.getObject()==-1) s.append("-\n");
            else s.append(String.format("%.1f\n", averages.getObject()));
        }
        if (getAverage()==-1) s.append("-\n");
        else s.append(String.format("%.1f\n", getAverage()));
        if (getTotalAverage()==-1) s.append("-");
        else s.append(String.format("%.1f", getTotalAverage()));
        return s.toString();
    }
    protected String getScores(){
        int points=main.getScore();
        StringBuilder s=new StringBuilder();
        scores.toFirst();
        while (scores.hasAccess()){
            s.append(String.format("%3d | %3d\n",points,scores.getObject() ));
            points-=scores.getObject();
            scores.next();
        }
        s.append(String.format("%3d",points));
        return s.toString();
    }
    protected String getAveragesPlus(){
        StringBuilder s=new StringBuilder();
        s.append("Averages\n");
        int i=1;
        for (averages.toFirst();averages.hasAccess();averages.next()){
            if (averages.getObject()==-1) s.append("-\n");
            else s.append(String.format("leg %d %5.1f\n", i,averages.getObject()));
            i++;
        }
        if (getAverage()==-1) s.append(String.format("leg %d   -\n",i));
        else s.append(String.format("leg %d %5.1f\n",i, getAverage()));
        if (getTotalAverage()==-1) s.append("tot     -");
        else s.append(String.format("tot   %5.1f", getTotalAverage()));
        return s.toString();
    }
}
