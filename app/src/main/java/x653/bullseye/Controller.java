package x653.bullseye;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.content.res.ResourcesCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import x653.bullseye.listenklassen.List;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

public class Controller implements DialogInterface.OnClickListener{
    private Player[] scores=new Player[3];
    private int legs[]=new int[3];
    private int modus;
    private int dran;
    private static final int M_neu=2;
    private static final int M_scores=3;
    private static final int M_weiterscore=4;
    private static final int M_NeuerDartFaktor=5;
    private static final int M_NeuerDartWert=6;
    private static final int M_legwin=7;
    private int[] dartscore=new int[3];
    private int[] faktor=new int[3];
    private int dart;
    private List<Character> rechner;
    private String[][] checkout;
    private MainActivity main;

    public Controller(MainActivity m){
        main=m;
        checkout=new String[3][];
        checkout[0] = main.getResources().getStringArray(R.array.checkout);
        checkout[1] = main.getResources().getStringArray(R.array.twodart);
        checkout[2] = main.getResources().getStringArray(R.array.onedart);

        scores[0]=new Player(main);
        scores[1]=new Player(main);
        scores[2]=new Player(main);
        rechner=new List<>();
        newGame();
    }
    protected int getLegNr(){
        int l=1;
        for (int i=0;i<3;i++){
            l+=legs[i];
        }
        if (modus==M_legwin) l--;
        return l;
    }
    private boolean isEmpty(){
        for (int i=0;i<main.getPlayer();i++){
            if (!scores[i].isEmpty()) return false;
        }
        return true;
    }
    protected Player getPlayer(int i){
        return scores[i];
    }
    protected void newGame(){
        for (int i=0;i<3;i++){
            scores[i].newGame();
            legs[i]=0;
        }
        initRechner();
        dran=0;
        modus=M_neu;
        drawAllScores();
        main.drawLegs(legs);
    }
    private void enterScore(){
        scores[dran].score(getEingabe());
        initRechner();
        if (scores[dran].isFinished()) {
            gameShot();
            legs[dran]++;
            main.drawLegs(legs);
            drawScore();
            modus=M_legwin;
        } else {
            drawScore();
            next();
            drawInput();
            modus=M_scores;
        }
    }
    private void delete(){
        android.support.v7.app.AlertDialog alrt = new android.support.v7.app.AlertDialog.Builder(main).create();
        alrt.setTitle("Delete");
        alrt.setMessage("Clear last or all score?");
        alrt.setButton(AlertDialog.BUTTON_NEGATIVE,"All", this);
        alrt.setButton(AlertDialog.BUTTON_NEUTRAL,"Cancel", this);
        alrt.setButton(AlertDialog.BUTTON_POSITIVE,"Last", this);
        alrt.show();
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: // yes
                initRechner();
                if (modus==M_legwin){
                    scores[dran].clearLastScore();
                    legs[dran]--;
                    main.drawLegs(legs);
                    drawInput();
                    modus = M_scores;
                } else if (modus==M_scores){
                    drawScore();
                    prev();
                    if (scores[dran].isEmpty()) next();
                    else scores[dran].clearLastScore();
                    drawInput();
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE: // no
                newGame();
                break;
            case DialogInterface.BUTTON_NEUTRAL: // neutral
                break;
            default:
                break;
        }
    }
    private void newLeg(){
        initRechner();
        for(int i=0;i<3;i++) scores[i].newLeg();
        dran=getLegNr()%main.getPlayer();
        drawAllScores();
        drawInput();
        modus=M_scores;
        gameOn();
    }
    private void initRechner(){
        dart=0;
        faktor[0]=1;
        faktor[1]=1;
        faktor[2]=1;
        dartscore[0]=0;
        dartscore[1]=0;
        dartscore[2]=0;
        rechner=new List<>();
    }
    private void next(){
        dran=(dran+1)%main.getPlayer();
    }
    private void prev(){
        dran=(dran+main.getPlayer()-1)%main.getPlayer();
    }
    private int getEingabe(){
        return faktor[0]*dartscore[0]+faktor[1]*dartscore[1]+faktor[2]*dartscore[2];
    }
    private boolean ungueltig1(int i){
        if (i==25) return false;
        if (i>20) return true;
        return false;
    }
    private boolean ungueltig3(int i){
        if (i==180) return false;
        if (i==177) return false;
        if (i==174) return false;
        if (i==171) return false;
        if (i==170) return false;
        if (i>170) return true;
        if (i==169) return true;
        if (i==166) return true;
        return false;
    }
    private boolean isOut(int i){
        if (i==170) return true;
        if (i==167) return true;
        if (i==164) return true;
        if (i==161) return true;
        if (i==160) return true;
        if (i>=159) return false;
        return true;
    }

    protected void in(char c){
        switch (modus) {
            case M_neu:
                modus=M_scores;
                gameOn();
                break;

            case M_scores:
                if (Character.isDigit(c)){
                    int i=c-'0';
                    dartscore[0]=i;
                    drawEingabe();
                    modus=M_weiterscore;
                } else if (c=='O') {
                    enterScore();
                } else if (c=='C') {
                    delete();
                } else if (c=='T') {
                    faktor[0]=3;
                    rechner.append('T');
                    drawRechner();
                    modus=M_NeuerDartFaktor;
                } else if (c=='D') {
                    faktor[0]=2;
                    rechner.append('D');
                    drawRechner();
                    modus=M_NeuerDartFaktor;
                }
                break;

            case M_NeuerDartFaktor:
                if (c=='C'){
                    rechner.toLast();
                    if (rechner.getObject()=='T'){
                        faktor[dart]=1;
                        rechner.remove();
                        rechner.toLast();
                        if (rechner.isEmpty()) {
                            drawInput();
                            modus=M_scores;
                        } else drawRechner();
                    } else if (rechner.getObject()=='D'){
                        faktor[dart]=1;
                        rechner.remove();
                        rechner.toLast();
                        if (rechner.isEmpty()) {
                            drawInput();
                            modus=M_scores;
                        } else drawRechner();
                    } else if (rechner.getObject()=='+'){
                        dart--;
                        rechner.remove();
                        drawRechner();
                        modus=M_NeuerDartWert;
                    }
                } else if (c=='T'){
                    if (faktor[dart]==1){
                        faktor[dart]=3;
                        rechner.append(c);
                        drawRechner();
                    } else if (faktor[dart]==2) {
                        faktor[dart]=3;
                        rechner.toLast();
                        rechner.setObject(c);
                        drawRechner();
                    } else if (faktor[dart]==3) {
                        faktor[dart]=1;
                        rechner.toLast();
                        rechner.remove();
                        if (rechner.isEmpty()) {
                            drawInput();
                            modus=M_scores;
                        } else drawRechner();
                    }
                } else if (c=='D'){
                    if (faktor[dart]==1) {
                        faktor[dart]=2;
                        rechner.append(c);
                        drawRechner();
                    } else if (faktor[dart]==2) {
                        faktor[dart]=1;
                        rechner.toLast();
                        rechner.remove();
                        if (rechner.isEmpty()) {
                            drawInput();
                            modus=M_scores;
                        } else drawRechner();
                    } else if (faktor[dart]==3) {
                        faktor[dart]=2;
                        rechner.toLast();
                        rechner.setObject(c);
                        drawRechner();
                    }
                } else if (Character.isDigit(c)){
                    int i=c-'0';
                    dartscore[dart] = i;
                    rechner.append(c);
                    drawRechner();
                    modus = M_NeuerDartWert;
                }
                break;

            case M_NeuerDartWert:
                if (Character.isDigit(c)){
                    int i=c-'0';
                    if (!((i==0)&&(dartscore[dart]==0))){
                        dartscore[dart]=dartscore[dart]*10+i;
                        rechner.append(c);
                        if ((faktor[dart]>1)&&(ungueltig1(dartscore[dart]))){
                            toast(String.format("%d - invalid score",dartscore[dart]));
                            dartscore[dart]=dartscore[dart]/10;
                            rechner.toLast();
                            rechner.remove();
                        }
                        if ((faktor[dart]==1)&&(ungueltig3(dartscore[dart]))){
                            toast(String.format("%d - invalid score",dartscore[dart]));
                            dartscore[dart]=dartscore[dart]/10;
                            rechner.toLast();
                            rechner.remove();
                        }
                        drawRechner();
                    }
                } else if (c=='C') {
                    if (dartscore[dart]>=0) {
                        dartscore[dart]=dartscore[dart]/10;
                        rechner.toLast();
                        rechner.remove();
                        rechner.toLast();
                        if (rechner.isEmpty()) {
                            drawInput();
                            modus=M_scores;
                        }
                        else {
                            if ((rechner.getObject()=='+')||(rechner.getObject()=='T') ||(rechner.getObject()=='D'))
                                modus=M_NeuerDartFaktor;
                            drawRechner();
                        }
                    }
                } else if (c=='O') {
                    enterScore();
                } else if (c=='+') {
                    if (dart<2) {
                        dart++;
                        rechner.append(c);
                        drawRechner();
                        modus=M_NeuerDartFaktor;
                    }
                } else if (c=='T') {
                    if (dart<2) {
                        dart++;
                        rechner.append('+');
                        rechner.append(c);
                        faktor[dart]=3;
                        drawRechner();
                        modus=M_NeuerDartFaktor;
                    }
                } else if (c=='D') {
                    if (dart<2) {
                        dart++;
                        rechner.append('+');
                        rechner.append(c);
                        faktor[dart]=2;
                        drawRechner();
                        modus=M_NeuerDartFaktor;
                    }
                }
                break;

            case M_weiterscore:
                if (Character.isDigit(c)){
                    int i=c-'0';
                    dartscore[0]=dartscore[0]*10+i;
                    if (ungueltig3(dartscore[0])) {
                        toast(String.format("%d - invalid score",dartscore[0]));
                        dartscore[0] = dartscore[0] / 10;
                    }
                    drawEingabe();
                } else if (c=='O') {
                    enterScore();
                } else if (c=='C') {
                    dartscore[0]=dartscore[0]/10;
                    if (dartscore[0]==0) {
                        drawInput();
                        modus=M_scores;
                    }else drawEingabe();
                } else if (c=='+') {
                    dart++;
                    String s=Integer.toString(dartscore[0]);
                    for (int i=0;i<s.length();i++){
                        rechner.append(s.charAt(i));
                    }
                    rechner.append(c);
                    drawRechner();
                    modus=M_NeuerDartFaktor;
                }else if (c=='T') {
                    dart++;
                    String s=Integer.toString(dartscore[0]);
                    for (int i=0;i<s.length();i++){
                        rechner.append(s.charAt(i));
                    }
                    rechner.append('+');
                    rechner.append(c);
                    faktor[dart]=3;
                    drawRechner();
                    modus=M_NeuerDartFaktor;
                }else if (c=='D') {
                    dart++;
                    String s=Integer.toString(dartscore[0]);
                    for (int i=0;i<s.length();i++){
                        rechner.append(s.charAt(i));
                    }
                    rechner.append('+');
                    rechner.append(c);
                    faktor[dart]=2;
                    drawRechner();
                    modus=M_NeuerDartFaktor;
                }
                break;
            case M_legwin:
                if (c=='O') {
                    newLeg();
                } else if (c=='C') {
                    delete();
                }   break;
            default:
                break;
        }
    }

    protected void hint(){
        switch (modus){
            case M_neu:
                drawInput();
                modus=M_scores;
                gameOn();
                break;
            case M_scores:
                if (isEmpty()) gameOn();
                else if (isOut(scores[dran].getRest()))
                    toast(String.format("%s you require %d!",main.getPlayerName(dran), scores[dran].getRest()));
                else toast(String.format("%s to throw",main.getPlayerName(dran)));
                break;
            case M_legwin:
                toast("Press ENTER to start a new Leg");
                break;
            default:
                toast("Press ENTER to continue");
                break;
        }
    }

    protected void toast(String s){
        Toast toast = Toast.makeText(main, s, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    protected void gameShot(){
        Toast toast = Toast.makeText(main,
                String.format("Game shot and the %s leg - %s!",legnr(),main.getPlayerName(dran)),
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private String legnr(){
        int l=getLegNr();
        String[] legs = main.getResources().getStringArray(R.array.legs);
        String legnr = String.format("%d.", l);
        if (l <= 10) legnr = legs[l - 1];
        return legnr;
    }
    protected void gameOn(){
        Toast toast = Toast.makeText(main,
                String.format("%s leg - it's %s to throw first\nGame on!", legnr(), main.getPlayerName(dran)),
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }


    private void drawAllScores(){
        for (int i=0;i<3;i++){
            drawScore(i);
        }
    }
    private void drawScore(int i){
        main.drawCards(i,new SpannableString(checkout(i)));
        String sc=String.format("%s      ",scores[i].getScores());
        main.drawScores(i,durchstreichen(sc));
    }
    private void drawScore(){
        drawScore(dran);
    }
    private void drawInput(){
        main.drawCards(dran,new SpannableString(checkout(dran)));
        String sc;
        sc=String.format("%s | ___",scores[dran].getScores());
        SpannableString s=durchstreichen(sc);
        main.drawScores(dran,s);
    }
    private void drawEingabe(){
        String sc;
        sc=String.format("%s | %3d\n%3d      ",
                scores[dran].getScores(),getEingabe(),
                scores[dran].getRest()-getEingabe());
        SpannableString s=durchstreichen(sc);
        s.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(main.getResources(), R.color.chalk2, null)),
            s.length() - 9, s.length(), 0);
            s.setSpan(new UnderlineSpan(), s.length() - 13, s.length() - 10, 0);
        main.drawScores(dran,s);
    }
    private void drawRechner(){
        String c=checkout(dran);
        String r=String.format("%s_ = %d",rechner.toString(),getEingabe());
        String all=r;
        if (c.length()!=0)
            all=String.format("%s\n%s",r,c);
        SpannableString s=new SpannableString(all);
        s.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(main.getResources(), R.color.chalk2, null)),
                0, r.length(), 0);
        main.drawCards(dran,s);
        drawEingabe();
    }
    private SpannableString durchstreichen(String sc){
        SpannableString s=new SpannableString(sc);
        int n=s.length()/10;
        for (int j=0;j<n;j++){
            if (s.charAt(j*10)!=' ') s.setSpan(new StrikethroughSpan(), j*10, j*10+3, 0);
            else if (s.charAt(j*10+1)!=' ') s.setSpan(new StrikethroughSpan(), j*10+1, j*10+3, 0);
            else if (s.charAt(j*10+2)!=' ') s.setSpan(new StrikethroughSpan(), j*10+2, j*10+3, 0);
        }
        return s;
    }
    private String checkout(int pl){
        int i=scores[pl].getRest();
        for (int d=0;d<dart;d++){
            i-=faktor[d]*dartscore[d];
        }
        String s="";
        switch (dart){
            case 0:
                if ((i>=2)&&(i<=170)) s=checkout[0][i];
                break;
            case 1:
                if ((i>=2)&&(i<=110)) s=checkout[1][i];
                break;
            case 2:
                if ((i>=2)&&(i<=50)) s=checkout[2][i];
                break;
        }
        return s;
    }

}
