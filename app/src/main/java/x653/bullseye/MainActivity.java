package x653.bullseye;

/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Controller d;
    private TextView[] legs=new TextView[3];
    private TextView[] names=new TextView[3];
    private TextView[] cards=new TextView[3];
    private TextView[] scores=new TextView[3];
    private TextView[] striche=new TextView[3];
    private TextView[] legLabel=new TextView[2];
    private int player;
    private int score;
    private Toast backtoast;

    private void SavePreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("player", player);
        editor.putInt("score", score);
        editor.commit();   // I missed to save the data to preference here,.
    }
    private void LoadPreferences() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        score = sharedPreferences.getInt("score", 501);
        player = sharedPreferences.getInt("player", 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        legs[0] = (TextView) findViewById(R.id.legs1);
        legs[1] = (TextView) findViewById(R.id.legs2);
        legs[2] = (TextView) findViewById(R.id.legs3);

        scores[0] = (TextView) findViewById(R.id.home);
        scores[1] = (TextView) findViewById(R.id.away);
        scores[2] = (TextView) findViewById(R.id.player3score);

        cards[0] = (TextView) findViewById(R.id.homecard);
        cards[1] = (TextView) findViewById(R.id.awaycard);
        cards[2] = (TextView) findViewById(R.id.player3card);

        names[0] = (TextView) findViewById(R.id.player1);
        names[1] = (TextView) findViewById(R.id.player2);
        names[2] = (TextView) findViewById(R.id.player3);

        striche[0] = (TextView) findViewById(R.id.strich1);
        striche[1] = (TextView) findViewById(R.id.strich2);
        striche[2] = (TextView) findViewById(R.id.strich3);

        legLabel[0] = (TextView) findViewById(R.id.labelleg1);
        legLabel[1] = (TextView) findViewById(R.id.labelleg2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LoadPreferences();
        setPlayer(player);
        d = new Controller(this);
    }

    protected void drawLegs(int[] l){
        switch (player){
            case 1:
                legs[0].setText(String.format("%d",l[0]));
                break;
            case 2:
                legs[0].setText(String.format("%d:%d",l[0],l[1]));
                break;
            case 3:
                legs[0].setText(String.format("%d",l[0]));
                legs[1].setText(String.format("%d",l[1]));
                legs[2].setText(String.format("%d",l[2]));
                break;
        }
    }
    protected CharSequence getPlayerName(int i){
        CharSequence s=names[i].getText();
        if (s.length() == 0) s = names[i].getHint();
        return s;
    }
    protected int getPlayer(){
        return player;
    }
    protected int getScore(){
        return score;
    }
    protected void drawScores(int i,SpannableString s){
        scores[i].setText(s, TextView.BufferType.SPANNABLE);
        if (getPlayer()==1) scores[1].setText(d.getPlayer(0).getAveragesPlus());
    }
    protected void drawCards(int i,SpannableString s){
        cards[i].setText(s,TextView.BufferType.SPANNABLE);
        if ((cards[0].getText().length()==0)&&
                (cards[1].getText().length()==0)&&
                (cards[2].getText().length()==0)) findViewById(R.id.id_cards).setVisibility(View.GONE);
        else findViewById(R.id.id_cards).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        SavePreferences();
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(backtoast!=null&&backtoast.getView().getWindowToken()!=null) {
                finish();
                super.onBackPressed();
            } else {
                backtoast = Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT);
                backtoast.show();
            }
        }
    }


    protected void hint(View view){
        findViewById(R.id.activity_main).requestFocus();
        d.hint();
    }
    protected void sendMessage(View view) {
        findViewById(R.id.activity_main).requestFocus();
        int taste = view.getId();
        switch (taste) {
            case R.id.button_clear:
                d.in('C');
                break;
            case R.id.button_OK:
                d.in('O');
                break;
            case R.id.button_stat:
                d.hint();
                break;
            case R.id.button_add:
                d.in('+');
                break;
            default:
                d.in(((Button) view).getText().charAt(0));
                break;
        }
        ScrollView sv = (ScrollView) findViewById(R.id.sv1);
        sv.scrollTo(0, sv.getBottom());
        findViewById(R.id.activity_main).requestFocus();
    }
    protected void setPlayer(int n){
        switch (n){
            case 1:
                player=1;
                legs[1].setVisibility(View.GONE);
                legs[2].setVisibility(View.GONE);
                names[1].setVisibility(View.GONE);
                names[2].setVisibility(View.GONE);
                names[0].setHint("HOME");
                names[1].setHint("AWAY");
                cards[2].setVisibility(View.GONE);
                scores[1].setGravity(Gravity.LEFT);
                scores[2].setVisibility(View.GONE);
                striche[0].setVisibility(View.GONE);
                striche[1].setVisibility(View.GONE);
                striche[2].setVisibility(View.GONE);
                legLabel[0].setVisibility(View.GONE);
                legLabel[1].setVisibility(View.GONE);
                break;
            case 2:
                player=2;
                legs[1].setVisibility(View.GONE);
                legs[2].setVisibility(View.GONE);
                names[1].setVisibility(View.VISIBLE);
                names[2].setVisibility(View.GONE);
                names[0].setHint("HOME");
                names[1].setHint("AWAY");
                cards[2].setVisibility(View.GONE);
                scores[1].setGravity(Gravity.CENTER_HORIZONTAL);
                scores[2].setVisibility(View.GONE);
                striche[0].setVisibility(View.GONE);
                striche[1].setVisibility(View.GONE);
                striche[2].setVisibility(View.GONE);
                legLabel[0].setVisibility(View.GONE);
                legLabel[1].setVisibility(View.GONE);
                break;
            case 3:
                player=3;
                legs[1].setVisibility(View.VISIBLE);
                legs[2].setVisibility(View.VISIBLE);
                names[1].setVisibility(View.VISIBLE);
                names[2].setVisibility(View.VISIBLE);
                names[0].setHint("Player1");
                names[1].setHint("Player2");
                cards[2].setVisibility(View.VISIBLE);
                scores[1].setGravity(Gravity.CENTER_HORIZONTAL);
                scores[2].setVisibility(View.VISIBLE);
                striche[0].setVisibility(View.VISIBLE);
                striche[1].setVisibility(View.VISIBLE);
                striche[2].setVisibility(View.VISIBLE);
                legLabel[0].setVisibility(View.VISIBLE);
                legLabel[1].setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case (R.id.nav_about):
                View view = LayoutInflater.from(this).inflate(R.layout.about, null);

                String versionName=BuildConfig.VERSION_NAME;
                int versionCode=BuildConfig.VERSION_CODE;
                if (versionName!=null)
                    ((TextView)view.findViewById(R.id.version))
                            .setText(String.format("%s (%d)",versionName,versionCode));

                AlertDialog alrt = new AlertDialog.Builder(this).setView(view).create();
                alrt.setTitle(R.string.about_title);
                alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                alrt.show();
                break;
            case (R.id.menu_stat):
                View view_stat = LayoutInflater.from(this).inflate(R.layout.statistics, null);
                AlertDialog stat_alrt = new AlertDialog.Builder(this).setView(view_stat).create();
                drawStats(view_stat);

                stat_alrt.setTitle("Statistics");
                stat_alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                stat_alrt.show();
                break;
            case (R.id.nav_help):
                View set = LayoutInflater.from(this).inflate(R.layout.help, null);

                AlertDialog alert = new AlertDialog.Builder(this).setView(set).create();
                alert.setTitle("Help");
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                alert.show();
                break;
            case (R.id.nav_settings):
                View setting = LayoutInflater.from(this).inflate(R.layout.settings, null);
                final RadioButton one=((RadioButton)setting.findViewById(R.id.radioButtonOne));
                final RadioButton two=((RadioButton)setting.findViewById(R.id.radioButtonTwo));
                final RadioButton three=((RadioButton)setting.findViewById(R.id.radioButtonThree));
                if (player==2) two.setChecked(true);
                else if (player==3) three.setChecked(true);
                else one.setChecked(true);
                final TextView points=((TextView)setting.findViewById(R.id.points));
                ((SeekBar)setting.findViewById(R.id.seekBar)).setOnSeekBarChangeListener(
                        new SeekBar.OnSeekBarChangeListener(){
                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                                // TODO Auto-generated method stub
                                points.setText(Integer.toString(100*progress+101));
                            }
                        }
                );
                ((SeekBar)setting.findViewById(R.id.seekBar)).setProgress((score-1)/100-1);

                AlertDialog alertset = new AlertDialog.Builder(this).setView(setting).create();
                alertset.setTitle("Settings");
                alertset.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (whichButton== AlertDialog.BUTTON_POSITIVE){
                                    score=Integer.parseInt(points.getText().toString());
                                    if (two.isChecked()) setPlayer(2);
                                    else if (three.isChecked()) setPlayer(3);
                                    else setPlayer(1);
                                    d.newGame();
                                }
                            }
                        });
                alertset.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });

                alertset.show();
                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }
    protected void drawStats(View view_stat){
        StringBuilder s=new StringBuilder();
        for (int i=1;i<=d.getLegNr();i++){
            s.append(String.format("leg %d\n",i));
        }
        s.append("total");
        ((TextView) view_stat.findViewById(R.id.stat_legs)).setText(s.toString());
        Player pl=d.getPlayer(0);
        ((TextView) view_stat.findViewById(R.id.stat_p1)).
                setText(getPlayerName(0));
        ((TextView) view_stat.findViewById(R.id.stat_legs1)).
                setText(pl.getAverages());
        ((TextView) view_stat.findViewById(R.id.stat_high1)).
                setText(pl.getHighest_finish());
         pl=d.getPlayer(1);
        ((TextView) view_stat.findViewById(R.id.stat_p2)).
                setText(getPlayerName(1));
        ((TextView) view_stat.findViewById(R.id.stats_legs2)).
                setText(pl.getAverages());
        ((TextView) view_stat.findViewById(R.id.stat_high2)).
                setText(pl.getHighest_finish());

        if (player==3) {
             pl=d.getPlayer(2);

            ((TextView) view_stat.findViewById(R.id.stat_p3)).
                    setText(getPlayerName(2));
            ((TextView) view_stat.findViewById(R.id.stats_legs3)).
                    setText(pl.getAverages());
            ((TextView) view_stat.findViewById(R.id.stat_high3)).
                    setText(pl.getHighest_finish());
        } else {
            view_stat.findViewById(R.id.stat_p3).setVisibility(View.GONE);
            view_stat.findViewById(R.id.stats_legs3).setVisibility(View.GONE);
            view_stat.findViewById(R.id.stat_high3).setVisibility(View.GONE);
        }
    }
}